package cl.vvalencia.registeringservice.service

import org.springframework.stereotype.Service
import cl.vvalencia.registeringservice.repository.EmailRepository
import cl.vvalencia.registeringservice.domain.EmailEntity
import org.springframework.beans.factory.annotation.Autowired

@Service
class EmailService {

	@Autowired
	lateinit var emailRepository: EmailRepository

	fun save(emailEntity: EmailEntity): EmailEntity =
			emailRepository.save(emailEntity)
	
	fun findAll(): List<EmailEntity> {
		return emailRepository.findAll()
	}


}