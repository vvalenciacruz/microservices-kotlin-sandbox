package cl.vvalencia.registeringservice.repository

import org.springframework.data.jpa.repository.JpaRepository
import cl.vvalencia.registeringservice.domain.EmailEntity

interface EmailRepository : JpaRepository<EmailEntity, Long>