package cl.vvalencia.registeringservice.controller

import cl.vvalencia.registeringservice.domain.EmailEntity
import cl.vvalencia.registeringservice.service.EmailService
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.beans.factory.annotation.Autowired

@RestController
@RequestMapping(path = arrayOf("/api/email"))
class ApiController {

	@Autowired
	lateinit var emailService: EmailService

	@PostMapping
	fun saveEmail(@RequestBody emailEntity: EmailEntity): EmailEntity {
		return emailService.save(emailEntity)
	}
	
	@GetMapping
	fun getAll(): List<EmailEntity> {
		return emailService.findAll()
	}
}