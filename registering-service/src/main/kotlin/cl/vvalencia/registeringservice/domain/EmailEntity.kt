package cl.vvalencia.registeringservice.domain

import javax.persistence.Entity
import javax.persistence.Table
import javax.persistence.Id
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType

@Entity
@Table(name = "emails")
data class EmailEntity(@Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Long, val mailTo: String, val subject: String, val body: String) 