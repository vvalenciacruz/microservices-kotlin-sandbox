package cl.vvalencia.registeringservice

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class RegisteringServiceApplication

fun main(args: Array<String>) {
    SpringApplication.run(RegisteringServiceApplication::class.java, *args)
}
